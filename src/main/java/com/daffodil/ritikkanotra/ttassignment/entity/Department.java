package com.daffodil.ritikkanotra.ttassignment.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Department {

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long departmentId;
    @Id
    private String departmentCode;
    private String departmentName;


    @ManyToMany(
//            cascade = CascadeType.ALL,
            mappedBy = "departments"
//            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private List<Employee> employees = new ArrayList<>();


    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

}
