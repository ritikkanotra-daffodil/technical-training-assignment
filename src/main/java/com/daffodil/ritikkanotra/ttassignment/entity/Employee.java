package com.daffodil.ritikkanotra.ttassignment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Employee {

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long employeeId;
    @Id
    private String employeeCode;
    private String employeeName;
    private String employeeDesignation;
    private String employeeEmail;
    // TODO Add Date employeeRegDate here
    @ManyToMany(
            cascade = CascadeType.ALL
//            fetch = FetchType.LAZY
    )
    @JoinTable(
            name = "employee_department_map",
            joinColumns = @JoinColumn (
                    name="employee_code",
                    referencedColumnName = "employeeCode"
            ),
            inverseJoinColumns = @JoinColumn (
                    name = "department_code",
                    referencedColumnName = "departmentCode"
            )
    )
    private List<Department> departments = new ArrayList<>();

    public void addDepartment(Department department) {
        departments.add(department);
    }



}
